//
//  RepositoryWrapObject.h
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositoryObject.h"
#import "BWJSONValueObject.h"

@interface RepositoryWrapObject : NSObject

@property(nonatomic, retain) NSArray<RepositoryObject*>* items;

@end
