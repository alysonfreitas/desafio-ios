//
//  RepositoryViewController.m
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import "RepositoryViewController.h"
#import "RepositoryTableViewCell.h"
#import "BWJSONMatcher.h"
#import "RepositoryWrapObject.h"
#import "PullRequestViewController.h"
#import "WS.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface RepositoryViewController ()
{
    RepositoryWrapObject *repositories;
    int currentPage;
    bool isLoading;
}
@end

@implementation RepositoryViewController

NSString * const kRepositoryCellIdentifier = @"RepositoryTableViewCell";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setEstimatedRowHeight:60];
    [self.tableView registerNib:[RepositoryTableViewCell getNibReference]
    forCellReuseIdentifier:kRepositoryCellIdentifier];
    
    [self firstLoad];
}

-(void)firstLoad{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.tableView setAlpha:0];
    currentPage = 1;
    isLoading = NO;
    [self load];
}

-(void)load
{
    if(isLoading)
    {
        return;
    }
    
    isLoading = YES;
    [WS getRepositories:^(NSString *response, int page) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.tableView setAlpha:1];

        if(response != nil)
        {
            if(page == currentPage)
            {
                NSLog(@"Page: %d was loaded.", page);
                if(page == 1)
                {
                    repositories = [BWJSONMatcher matchJSONString:response withClass:[RepositoryWrapObject class]];
                }
                else
                {
                    RepositoryWrapObject *temp = [BWJSONMatcher matchJSONString:response withClass:[RepositoryWrapObject class]];
                    repositories.items = [repositories.items arrayByAddingObjectsFromArray:temp.items];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                });
                currentPage += 1;
            }
        }
        isLoading = NO;
    } page:currentPage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return repositories.items.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RepositoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRepositoryCellIdentifier];
    [cell load:[repositories.items objectAtIndex:indexPath.row]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PullRequestViewController *pullRequestViewController =
    [self.storyboard instantiateViewControllerWithIdentifier:@"PullRequestViewController"];
    pullRequestViewController.repository = [repositories.items objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:pullRequestViewController animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView_
{
    CGFloat actualPosition = scrollView_.contentOffset.y;
    CGFloat contentHeight = scrollView_.contentSize.height - (3 * self.tableView.frame.size.height)/2;
    if (actualPosition >= contentHeight) {
        [self load];
    }
}

@end
