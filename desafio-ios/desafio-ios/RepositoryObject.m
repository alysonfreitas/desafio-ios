//
//  RepositoryObject.m
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import "RepositoryObject.h"

@interface RepositoryObject ()

@end

@implementation RepositoryObject

+ (NSDictionary *)propertyMapper {
    return @{@"desc": @"description"};
}

@end
