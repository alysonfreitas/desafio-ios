//
//  RepositoryObject.h
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserObject.h"
#import "BWJSONValueObject.h"

@interface RepositoryObject : NSObject<BWJSONValueObject>

@property(nonatomic, retain) NSString* name;
@property(nonatomic, retain) NSString* desc;
@property(nonatomic, retain) NSString* stargazers_count;
@property(nonatomic, retain) NSString* open_issues;
@property(nonatomic, retain) NSString* closed_issues;
@property(nonatomic, retain) NSString* forks;
@property(nonatomic, retain) NSString* full_name;
@property(nonatomic, retain) UserObject* owner;

@end
