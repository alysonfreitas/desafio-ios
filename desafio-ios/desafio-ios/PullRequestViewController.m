//
//  PullRequestViewController.m
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import "PullRequestViewController.h"
#import "PullRequestTableViewCell.h"
#import "BWJSONMatcher.h"
#import "WS.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "WebViewController.h"

@interface PullRequestViewController ()
{
    NSMutableArray<PullRequestObject*>* pullRequests;
}
@end

@implementation PullRequestViewController

NSString * const kPullRequestTableViewCell = @"PullRequestTableViewCell";
float const minHeightCell = 108.f;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:self.repository.name];
    [self.tableView setEstimatedRowHeight:1000];
    [self.tableView registerNib:[PullRequestTableViewCell getNibReference]
         forCellReuseIdentifier:kPullRequestTableViewCell];

    [self load];
}

-(void)load{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.tableView setAlpha:0];
    [WS getPullRequests:self.repository.full_name onCompletion:^(NSString *response) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.tableView setAlpha:1];
        pullRequests = [BWJSONMatcher matchJSONString:response withClass:[PullRequestObject class]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateState];
            [self.tableView reloadData];
        });
    }];
}

-(void)updateState{
    int opened = 0;
    int closed = 0;
    for(PullRequestObject *object in pullRequests)
    {
        if([object.state isEqualToString:@"open"])
        {
            opened++;
        }
        else
        {
            closed++;
        }
    }
    [openedLabel setText:[NSString stringWithFormat:NSLocalizedString(@"opened", nil), opened]];
    [closedLabel setText:[NSString stringWithFormat:NSLocalizedString(@"closed", nil), closed]];
    [UIView animateWithDuration:0.2 animations:^{
        [headerHeightConstraint setConstant:35];
        [self.view layoutIfNeeded];
    }];
}

-(IBAction)backButtonPressed:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return pullRequests.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PullRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPullRequestTableViewCell];
    [cell load:[pullRequests objectAtIndex:indexPath.row] tableView:self.tableView];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self openUrl:[pullRequests objectAtIndex:indexPath.row].html_url];
}

-(void)openUrl:(NSString*)url
{
    WebViewController *webViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    webViewController.url = url;
    webViewController.repositoryName = self.repository.name;
    [self.navigationController pushViewController:webViewController animated:YES];
}

@end
