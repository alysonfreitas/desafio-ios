//
//  RepositoryTableViewCell.h
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoryObject.h"

@interface RepositoryTableViewCell : UITableViewCell
{
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UILabel *forkNumberLabel;
    IBOutlet UILabel *starsNumberLabel;
    IBOutlet UIImageView *userImage;
    IBOutlet UILabel *usernameLabel;
}

+(UINib*)getNibReference;
-(void)load:(RepositoryObject*)object;

@end
