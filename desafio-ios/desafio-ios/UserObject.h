//
//  UserObject.h
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserObject : NSObject

@property(nonatomic, retain) NSString* login;
@property(nonatomic, retain) NSString* avatar_url;

@end
