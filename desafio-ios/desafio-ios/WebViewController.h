//
//  WebViewController.h
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *webView;
}

@property(nonatomic, retain) NSString *repositoryName;
@property(nonatomic, retain) NSString *url;
-(IBAction)backButtonPressed:(id)sender;

@end
