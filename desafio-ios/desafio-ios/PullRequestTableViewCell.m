//
//  PullRequestTableViewCell.m
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import "PullRequestTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation PullRequestTableViewCell

-(void)awakeFromNib {
    [super awakeFromNib];
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

+(UINib*)getNibReference{
    UINib *nib = [UINib nibWithNibName:@"PullRequestTableViewCell" bundle:nil];
    return nib;
}

-(NSString *)getFormattedDate:(NSString*)dateUnformatted
{
    NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
    [dateformat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *date = [dateformat dateFromString:dateUnformatted];
    [dateformat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
    return [dateformat stringFromDate:date];
}

-(void)load:(PullRequestObject*)object tableView:(UITableView*)tv{
    
    [titleLabel setText:object.title];
    [descriptionLabel setText:object.body];
    [usernameLabel setText:object.user.login];
    [dataLabel setText:[self getFormattedDate:object.created_at]];

    [userImage sd_setImageWithURL:[NSURL URLWithString:object.user.avatar_url]
    placeholderImage:[UIImage imageNamed:@"ic-avatar"]
    completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL){
        [self layoutIfNeeded];
        [userImage.layer setCornerRadius:userImage.frame.size.width/2];
        [userImage.layer setMasksToBounds:YES];
    }];
}


@end
