//
//  PullRequestObject.h
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BWJSONValueObject.h"
#import "UserObject.h"

@interface PullRequestObject : NSObject<BWJSONValueObject>

@property(nonatomic, retain) NSString* title;
@property(nonatomic, retain) NSString* body;
@property(nonatomic, retain) NSString* state;
@property(nonatomic, retain) NSString* created_at;
@property(nonatomic, retain) NSString* html_url;
@property(nonatomic, retain) UserObject* user;

@end
