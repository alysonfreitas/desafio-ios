//
//  PullRequestTableViewCell.h
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequestObject.h"
@interface PullRequestTableViewCell : UITableViewCell
{
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UIImageView *userImage;
    IBOutlet UILabel *usernameLabel;
    IBOutlet UILabel *dataLabel;
}

+(UINib*)getNibReference;
-(void)load:(PullRequestObject*)object tableView:(UITableView*)tv;

@end
