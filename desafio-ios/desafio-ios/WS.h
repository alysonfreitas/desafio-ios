//
//  WS.h
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WS : NSObject

+(void)getRepositories:(void (^)(NSString *response, int page))completion page:(int)page;
+(void)getPullRequests:(NSString*)fullName onCompletion:(void (^)(NSString *response))completion;


@end
