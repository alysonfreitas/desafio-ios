//
//  WS.m
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import "WS.h"
#import <AFNetworking/AFNetworking.h>

@implementation WS
NSString * const kBaseUrl = @"https://api.github.com";
NSString * const kRepositoryURL = @"%@/search/repositories?q=language:Java&sort=stars&page=%d";
NSString * const kPullsPrototypeURL = @"%@/repos/%@/pulls?state=all";

+(void)getRepositories:(void (^)(NSString *response, int page))completion page:(int)page{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:[NSString stringWithFormat:kRepositoryURL, kBaseUrl, page] parameters:nil progress:nil
    success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if(responseObject != nil)
        {
            NSString *json = [[NSString alloc] initWithData:
            [NSJSONSerialization dataWithJSONObject:responseObject options:0 error:nil]
            encoding:NSUTF8StringEncoding];
            completion(json, page);
        }
        else
        {
            completion(nil, page);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion(nil, page);
    }];
}

+(void)getPullRequests:(NSString*)fullName onCompletion:(void (^)(NSString *response))completion{
    NSString *pullsUrl = [NSString stringWithFormat:kPullsPrototypeURL, kBaseUrl, fullName];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:pullsUrl parameters:nil progress:nil
         success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
             if(responseObject != nil)
             {
                 NSString *json = [[NSString alloc] initWithData:
                 [NSJSONSerialization dataWithJSONObject:responseObject options:0 error:nil]
                 encoding:NSUTF8StringEncoding];
                 completion(json);
             }
             else
             {
                 completion(nil);
             }
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             completion(nil);
         }];
}

@end
