//
//  RepositoryTableViewCell.m
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import "RepositoryTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation RepositoryTableViewCell


+(UINib*)getNibReference{
    UINib *nib = [UINib nibWithNibName:@"RepositoryTableViewCell" bundle:nil];
    return nib;
}

-(void)load:(RepositoryObject*)object
{
    [titleLabel setText:object.name];
    [descriptionLabel setText:object.desc];
    [starsNumberLabel setText:object.stargazers_count];
    [forkNumberLabel setText:object.forks];
    [usernameLabel setText:object.owner.login];
    [userImage sd_setImageWithURL:[NSURL URLWithString:object.owner.avatar_url]
    placeholderImage:[UIImage imageNamed:@"ic-avatar"]
    completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL){
        [self layoutIfNeeded];
        [userImage.layer setCornerRadius:userImage.frame.size.width/2];
        [userImage.layer setMasksToBounds:YES];
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
