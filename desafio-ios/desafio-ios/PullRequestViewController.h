//
//  PullRequestViewController.h
//  desafio-ios
//
//  Created by Alyson Freitas on 23/12/16.
//  Copyright © 2016 Alyson Freitas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RepositoryObject.h"

@interface PullRequestViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UILabel *openedLabel;
    IBOutlet UILabel *closedLabel;
    IBOutlet NSLayoutConstraint *headerHeightConstraint;
}

@property(nonatomic, retain) IBOutlet UITableView *tableView;
@property(nonatomic, retain) RepositoryObject *repository;

-(IBAction)backButtonPressed:(id)sender;
@end
